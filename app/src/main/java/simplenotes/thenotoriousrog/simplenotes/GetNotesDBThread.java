package simplenotes.thenotoriousrog.simplenotes;

import android.util.JsonReader;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import simplenotes.thenotoriousrog.simplenotes.backend.Note;
import simplenotes.thenotoriousrog.simplenotes.backend.Utils;
import simplenotes.thenotoriousrog.simplenotes.interfaces.DBLiason;
import simplenotes.thenotoriousrog.simplenotes.interfaces.MainUpdater;
import simplenotes.thenotoriousrog.simplenotes.presenters.MainActivityPresenter;

public class GetNotesDBThread implements Runnable {

    private MainActivityPresenter presenter;
    private Thread backgroundThread;

    public GetNotesDBThread(MainActivityPresenter presenter) {
        this.presenter = presenter;
        this.backgroundThread = new Thread(this);
    }

    public void start() {
        backgroundThread.start();
    }

    // retrieves notes from the database for this user.
    private ArrayList<Note> retrieveNotes(InputStreamReader in) {
        JsonReader reader = new JsonReader(in);
        ArrayList<Note> notes = new ArrayList<>();

        try {
            //reader.beginObject();
            System.out.println("reader = " + reader.toString());
            reader.beginArray();
            int creatorId = -1;
            String subject = null;
            String noteText = null;
            System.out.println("reader object = " + reader.peek().toString());
            while(reader.hasNext()) {
                System.out.println("Peeking reader = " + reader.peek());
                if(reader.peek().toString().equals("BEGIN_OBJECT")) {
                    reader.beginObject();
                }
                String nextName = reader.nextName();
                System.out.println("Next name I'm looking at: " + nextName);
                if(nextName.equals("CreatorId")) {
                    creatorId = reader.nextInt();
                } else if(nextName.equals("Subject")) {
                    subject = reader.nextString();
                } else if( nextName.equals("NoteText")) {
                    noteText = reader.nextString();
                }

                // see if we have the necessary fields to create our Note object
                if(creatorId != -1 && subject != null && noteText != null) {
                    Note note = new Note(creatorId, subject, noteText);
                    notes.add(note); // add to our notes DB.

                    // reset note fields for more objects.
                    creatorId = -1;
                    subject = null;
                    noteText = null;
                    reader.endObject();
                }
            }

        } catch (IOException ex) {
            System.out.println("Encountered a problem when attempting to read json input.");
            ex.printStackTrace();
        }

        return notes;

    }

    @Override
    public void run()
    {
        presenter.startLoading(); // begin showing the progress bar.
        HttpURLConnection urlConnection;
        try {
            URL localHost = new URL(Utils.GET_NOTES_URL + "1");
            urlConnection = (HttpURLConnection) localHost.openConnection();
            urlConnection.setRequestMethod("GET");

            System.out.println("Get Note response code: " + urlConnection.getResponseCode());
            ArrayList<Note> notes = retrieveNotes(new InputStreamReader(urlConnection.getInputStream()));
            presenter.updateMain(notes);

        } catch (Exception ex) {
            System.out.println("Something broke while trying to access the url");
            ex.printStackTrace();
        }

        presenter.stopLoading(); // hide the progress bar.
    }

}
