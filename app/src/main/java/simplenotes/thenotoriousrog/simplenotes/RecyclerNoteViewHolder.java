package simplenotes.thenotoriousrog.simplenotes;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import simplenotes.thenotoriousrog.simplenotes.interfaces.NoteView;

public class RecyclerNoteViewHolder extends RecyclerView.ViewHolder implements NoteView {

    private View noteView;
    private TextView subjectText;
    private TextView noteText;

    public RecyclerNoteViewHolder(View noteView) {
        super(noteView);
        this.noteView = noteView;
        subjectText = noteView.findViewById(R.id.subject);
        noteText = noteView.findViewById(R.id.content);
    }

    @Override
    public void setSubject(String subject) {
        this.subjectText.setText(subject);
    }

    @Override
    public void setNote(String note) {
        this.noteText.setText(note);
    }
}
