package simplenotes.thenotoriousrog.simplenotes.presenters;

import java.util.ArrayList;

import simplenotes.thenotoriousrog.simplenotes.GetNotesDBThread;
import simplenotes.thenotoriousrog.simplenotes.backend.Note;
import simplenotes.thenotoriousrog.simplenotes.interfaces.MainUpdater;

public class MainActivityPresenter {

    private MainUpdater mainUpdater;
    private ArrayList<Note> notes = new ArrayList<>();
    private GetNotesDBThread liason;

    public MainActivityPresenter(MainUpdater mainUpdater) {
        this.mainUpdater = mainUpdater;

        liason = new GetNotesDBThread(this);
        liason.start(); // run the background thread which will update the main UI.
    }

    public void updateMain(ArrayList<Note> notes) {
        this.notes = notes;
        mainUpdater.update(notes);
    }

    public void startLoading() {
        mainUpdater.showProgressBar();
    }

    public void stopLoading() {
        mainUpdater.hideProgressBar();
    }

    // makes a call to get notes from the db.
    public void update() {
        liason = new GetNotesDBThread(this);
        liason.start();
    }

    // todo: the main activity presenter will be in charge of speaking with the backend database system to get an updated version of the cards after the user logs in
    // however, in the meantime I will be using this to create a very small list of array list notes item to populate into my app.

    // todo: the work for updating the main activity in the backend will occur here spacc' e vetrine

}
