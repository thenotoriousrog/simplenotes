package simplenotes.thenotoriousrog.simplenotes.presenters;

import java.util.ArrayList;

import simplenotes.thenotoriousrog.simplenotes.backend.Note;
import simplenotes.thenotoriousrog.simplenotes.interfaces.NoteView;

public class RecyclerNotesViewPresenter {

    private ArrayList<Note> notes;

    public RecyclerNotesViewPresenter(ArrayList<Note> notes) {
        this.notes = notes;
    }

    public void onBindNoteViewAtPosition(int pos, NoteView noteView) {
        Note note = notes.get(pos);

        noteView.setSubject(note.getSubject());
        noteView.setNote(note.getNote());
    }

    public int getNoteCount() {
        return notes.size();
    }

}
