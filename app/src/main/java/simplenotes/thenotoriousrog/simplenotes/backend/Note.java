package simplenotes.thenotoriousrog.simplenotes.backend;

import android.text.format.DateUtils;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

/*
    * Holds basic information about the note that the user is creating.
 */
public class Note {

    private final int creatorId;
    private final String subject;
    private final String note;

    public Note(int creatorId, String subject, String note) {
        this.creatorId = creatorId;
        this.subject = subject;
        this.note = note;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public String getSubject() {
        return subject;
    }

    public String getNote() {
        return note;
    }

}
