package simplenotes.thenotoriousrog.simplenotes.backend;

import simplenotes.thenotoriousrog.simplenotes.interfaces.MainUpdater;

/*
    * Utils for the rest of the application to use anywhere in the app.
 */
public class Utils {

    public static final String GET_NOTES_URL = "http://192.168.0.47:8082/notes/"; // use this to contruct our GET URL
    public static final String POST_URL = "http://192.168.0.47:8082/notes"; // use this to construct our POST URL

    private static MainUpdater updater;
    public static MainUpdater getMainUpdater() {
        return updater;
    }

    public static void setMainUpdater(MainUpdater mainUpdater) {
        updater = mainUpdater;
    }

}
