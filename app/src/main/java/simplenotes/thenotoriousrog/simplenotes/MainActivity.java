package simplenotes.thenotoriousrog.simplenotes;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;
import simplenotes.thenotoriousrog.simplenotes.backend.Note;
import simplenotes.thenotoriousrog.simplenotes.backend.Utils;
import simplenotes.thenotoriousrog.simplenotes.interfaces.DBLiason;
import simplenotes.thenotoriousrog.simplenotes.interfaces.MainUpdater;
import simplenotes.thenotoriousrog.simplenotes.presenters.MainActivityPresenter;
import simplenotes.thenotoriousrog.simplenotes.presenters.RecyclerNotesViewPresenter;

public class MainActivity extends AppCompatActivity implements MainUpdater {

    private MainActivityPresenter presenter;
    private RecyclerNotesViewAdapter adapter;
    private RecyclerView recyclerView;
    private FloatingActionButton createNoteButton;
    private WaveSwipeRefreshLayout swipeRefreshLayout;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.handler = new Handler();
        this.adapter = new RecyclerNotesViewAdapter(new RecyclerNotesViewPresenter(new ArrayList<Note>())); // ** Note sending an empty list to begin with until we update later....
        this.recyclerView = findViewById(R.id.mainRecyclerView);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        swipeRefreshLayout = findViewById(R.id.mainSwipeRefresh);
        swipeRefreshLayout.setWaveColor(ContextCompat.getColor(this, R.color.colorPrimary)); // make the colors match
        swipeRefreshLayout.setMaxDropHeight(80);
        swipeRefreshLayout.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.update();
            }
        });

        createNoteButton = findViewById(R.id.createNoteButton);
        createNoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CreateNoteActivity.class);
                startActivity(intent);
            }
        });

        this.presenter = new MainActivityPresenter(this); // Note: creating the presenter spawns a background thread.
        Utils.setMainUpdater(this); // set the main updater to be used throughout the rest of the application.
    }

    @Override
    public void showProgressBar() {
        if(Looper.myLooper() == null) {
            Looper.prepare();
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });

    }

    @Override
    public void hideProgressBar() {
        if(Looper.myLooper() == null) {
            Looper.prepare();
        }
//        swipeRefreshLayout.setRefreshing(false);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void update(ArrayList<Note> updatedNotes) {
        adapter = new RecyclerNotesViewAdapter(new RecyclerNotesViewPresenter(updatedNotes)); // create the new notes and update the recycler view.
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                recyclerView.invalidate();
            }
        });

    }

    @Override
    public void requestUpdate() {
        presenter.update(); // start a new thread and force the app to reload with new data.
    }


    @Override
    public void onRestart() {
        super.onRestart();
        presenter.update(); // have the presenter update the UI
    }
}
