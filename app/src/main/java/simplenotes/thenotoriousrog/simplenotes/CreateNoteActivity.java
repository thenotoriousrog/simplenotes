package simplenotes.thenotoriousrog.simplenotes;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import simplenotes.thenotoriousrog.simplenotes.backend.Utils;
import simplenotes.thenotoriousrog.simplenotes.interfaces.CreateNoteActivityUpdater;
import simplenotes.thenotoriousrog.simplenotes.interfaces.MainUpdater;

public class CreateNoteActivity extends AppCompatActivity implements CreateNoteActivityUpdater {

    private MainUpdater updater; // used to update the mainactivity upon new data being created from our background threads.
    private CreateNotePresenter presenter;
    private Button addNoteButton;
    private EditText subjectEditText;
    private EditText noteEditText;
    private RelativeLayout layout;

    private void setupSubjectEditTextWatcher(){
        subjectEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                presenter.setSubjectText(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) { }
        });
    }

    private void setupNoteEditTextWatcher() {
        noteEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                presenter.setNoteText(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_note_activity);

        ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.setTitle(getString(R.string.ActionBarTitle));
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        this.updater = Utils.getMainUpdater(); // get main updater from utils class.
        this.presenter = new CreateNotePresenter(updater, this);
        addNoteButton = findViewById(R.id.createNote);
        subjectEditText = findViewById(R.id.subjectEditText);
        noteEditText = findViewById(R.id.noteEditText);
        layout = findViewById(R.id.layout);

        setupSubjectEditTextWatcher();
        setupNoteEditTextWatcher();

        addNoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.addNoteClicked();
            }
        });

    }

    @Override
    public void displayErrorSnackbar() {
        Snackbar.make(layout, getString(R.string.WarningSnackbar), Snackbar.LENGTH_SHORT ).show();
    }

    @Override
    public void kill() {
        this.finish();
    }
}
