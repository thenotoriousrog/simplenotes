package simplenotes.thenotoriousrog.simplenotes.interfaces;

import android.os.Handler;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import simplenotes.thenotoriousrog.simplenotes.RecyclerNotesViewAdapter;
import simplenotes.thenotoriousrog.simplenotes.backend.Note;

public interface MainUpdater {

    void showProgressBar();
    void hideProgressBar();
    void update(ArrayList<Note> updatedNotes); // updates the recycler view with an updated version of the notes that we are working with.
    void requestUpdate(); // calls the presenter to pull data from the MainActivityPresenter

}
