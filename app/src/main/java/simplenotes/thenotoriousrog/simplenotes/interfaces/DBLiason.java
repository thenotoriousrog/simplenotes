package simplenotes.thenotoriousrog.simplenotes.interfaces;

import java.util.ArrayList;

import simplenotes.thenotoriousrog.simplenotes.backend.Note;

public interface DBLiason {

    void updateNotes(ArrayList<Note> notes);
    void createNote(String status);

}
