package simplenotes.thenotoriousrog.simplenotes.interfaces;

public interface NoteView {

    void setSubject(String subject);
    void setNote(String note);
}
