package simplenotes.thenotoriousrog.simplenotes.interfaces;

public interface CreateNoteActivityUpdater {

    void displayErrorSnackbar(); // tell the user that they have to fill out both fields.
    void kill(); // kills the activity by calling its finish() method.

}
