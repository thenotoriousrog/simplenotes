package simplenotes.thenotoriousrog.simplenotes;

import simplenotes.thenotoriousrog.simplenotes.backend.Note;
import simplenotes.thenotoriousrog.simplenotes.interfaces.CreateNoteActivityUpdater;
import simplenotes.thenotoriousrog.simplenotes.interfaces.MainUpdater;

public class CreateNotePresenter {

    private MainUpdater updater;
    private CreateNoteActivityUpdater noteActivityUpdater;
    private String subjectText = "";
    private String noteText = "";
    private int creatorId = 1; // todo: remove this as it's hardcoded!

    public CreateNotePresenter(MainUpdater updater, CreateNoteActivityUpdater noteActivityUpdater) {
        this.updater = updater;
        this.noteActivityUpdater = noteActivityUpdater;
    }

    public void setSubjectText(String subjectText) {
        this.subjectText = subjectText;
    }

    public void setNoteText(String noteText) {
        this.noteText = noteText;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public void addNoteClicked() {

        if(subjectText.isEmpty() || noteText.isEmpty()) {
            noteActivityUpdater.displayErrorSnackbar();
        } else {
            Note note = new Note(creatorId, subjectText, noteText);
            AddNoteDBThread thread = new AddNoteDBThread(note);
            thread.start();
            noteActivityUpdater.kill();
        }


    }

}
