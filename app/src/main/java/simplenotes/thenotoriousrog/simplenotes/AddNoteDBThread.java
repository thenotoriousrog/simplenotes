package simplenotes.thenotoriousrog.simplenotes;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import simplenotes.thenotoriousrog.simplenotes.backend.Note;
import simplenotes.thenotoriousrog.simplenotes.backend.Utils;
import simplenotes.thenotoriousrog.simplenotes.interfaces.MainUpdater;

/*
    * A thread that will update the threads once a new note is created an added to the database.
 */
public class AddNoteDBThread implements Runnable {

    private Note note;
    private Thread backgroundThread;

    public AddNoteDBThread(Note note) {
        this.note = note;
        this.backgroundThread = new Thread(this);
    }

    public void start() {
        backgroundThread.start();
    }

    private String getURLPath() {
        String url = Utils.POST_URL + "?CreatorId=" + 1 + "&Subject=" + note.getSubject() + "&NoteText=" + note.getNote();
        return url;
    }

    // Send a request to the database to add a new item to the thread. Very important!
    @Override
    public void run()
    {
        HttpURLConnection urlConnection;
        try {
            //URL localHost = new URL("http://192.168.0.47:8082/notes?CreatorId=1&Subject=hardcodedSubject&NoteText=hardcodedNoteText");
            URL localHost = new URL(getURLPath());
            urlConnection = (HttpURLConnection) localHost.openConnection();

            urlConnection.setRequestMethod("POST");
//            urlConnection.setRequestProperty("CreatorId", Integer.toString(note.getCreatorId()));
//            urlConnection.setRequestProperty("Subject", note.getSubject());
//            urlConnection.setRequestProperty("NoteText", note.getNote());
            urlConnection.setRequestProperty("CreatorId", "1");
            urlConnection.setRequestProperty("Subject", "test1Subject");
            urlConnection.setRequestProperty("NoteText", "test1Note");
            //urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect(); // trigger the connection.

            int responseCode = urlConnection.getResponseCode();
            System.out.println("Response code after sending " + responseCode);
            System.out.println("Request method being sent " + urlConnection.getRequestMethod());
            System.out.println("Response Message returned: " + urlConnection.getResponseMessage());
            System.out.println("Request header: " + urlConnection.getHeaderFieldKey(1) + " " + urlConnection.getHeaderField(1));
            System.out.println("Url being posted: " + urlConnection.getURL().toString());

        } catch (Exception ex) {
            System.out.println("Something broke while trying to access the url");
            ex.printStackTrace();
        }

    }
}
