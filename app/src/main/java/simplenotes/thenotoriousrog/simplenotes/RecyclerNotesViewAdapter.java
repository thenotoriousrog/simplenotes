package simplenotes.thenotoriousrog.simplenotes;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import simplenotes.thenotoriousrog.simplenotes.presenters.RecyclerNotesViewPresenter;

public class RecyclerNotesViewAdapter extends RecyclerView.Adapter<RecyclerNoteViewHolder> {

    private final RecyclerNotesViewPresenter presenter;

    public RecyclerNotesViewAdapter(RecyclerNotesViewPresenter presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public RecyclerNoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerNoteViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.note_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerNoteViewHolder recyclerNoteViewHolder, int i ) {
        presenter.onBindNoteViewAtPosition(i, recyclerNoteViewHolder);
    }

    @Override
    public int getItemCount() {
        return presenter.getNoteCount();
    }
}
